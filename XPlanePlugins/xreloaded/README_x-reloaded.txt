X-Reloaded v.1.1b1 - (c) by barbarossa 2013
derived from
Reload_Scenery v.001 - (c) by barbarossa 2012

SHORT DESCRIPTION

Reload plugins, aircraft or scenery from one plugin without restarting X-Plane.

NOW with bindable commands for all three functions for very quick reloading.

DESCRIPTION

This is a replacement for my Relaod_Scenery plugin.

It now features plugin reloading and aircraft reloading as well.

Now we can do all our reloading from one place.

If you work a lot on multiple or complex projects that require lots of reloading, this may come in handy.

We do now have three menu items:

Reload plugins
Reload aircraft
Reload scenery

Reload plugins will call XPLMReloadPlugins(), just as the famous ReloadPlugins plugin does.

Reload aircraft will just do that, reload the current user aircraft.

Reload scenery:
Scenery designers can reload the scenery into X-Plane without restarting the sim.
The menu item "Plugins/Reload_Scenery/Reload scenery" will call XPLMReloadScenery().

Please note that an all new scenery pack just dropped into the Custom Scenery folder gets not registered without restarting X-Plane.
But most scenery edition will load fine with this plugin.

You can bind the commands

barbarossa/xreloaded/reload_aircraft
barbarossa/xreloaded/reload_plugins
barbarossa/xreloaded/reload_scenery

to any button or key.

SYSTEM REQUIREMENTS

OS: Linux+Mac+Windows+32+64 (tested on Vista Home Premium SP2 32Bit)
X-Plane 9+

INSTALLATION

Unzip the folder "xreloaded" into "X-Plane*/Resources/plugins/".

CHANGES

1.1b1
- added three commands to bind to keys or buttons for very quick reloading
