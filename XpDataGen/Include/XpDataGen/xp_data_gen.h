/**
 * @file xp_data_gen.h
 * @author Ojit Mehta (ojit.s.mehta@gmail.com)
 * @brief X Plane Dataset Generator
 * @version 0.1
 * @date 2021-05-09
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDefs.h"
#include "XPLMUtilities.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMCamera.h"
#include "XPLMPlanes.h"
#include "XPLMMenus.h"

/**
 * @brief 
 * 
 * @param outCameraPosition 
 * @param inIsLosingControl 
 * @param inRefcon 
 * @return int 
 */
int 	CameraControl(
	XPLMCameraPosition_t* outCameraPosition,
	int                  inIsLosingControl,
	void* inRefcon);

void MenuHandler(
	void* in_menu_ref,
	void* in_item_ref);

float CameraControlSeqAcf(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);

float InitStartupVar(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);


float ConfigureEnv(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);

float CreateRecorders(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);

float DestroyRecorders(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);
float PauseSimRec(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);


float UnPauseSimRec(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);
float	RecordDataCallback(
	float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);
float OwnshipCollider(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon);
static int CaptureScreenCb(XPLMDrawingPhase inPhase, int inIsBefore, void* inRefcon);
bool CaminitTimeDelta(float t);
bool CheckOutAcfs();

namespace xpd{
	enum CState {
		NONSEQ = 1,
		SEQCAMFIXED = 2,
		SEQACF = 3,
		USRS = 4,
		USRM = 5,
		USRC = 6,
		OWNC = 7,
		GLPIX = 8
	};

	/*Camera Location and Variables*/
	struct CameraVar {
		float x, y, z;
		float roll, pitch, head;
		float droll, dpitch, dhead;
		float vz;
		float cvx, cvy, cvz;

		float      init_time;
		bool       init;
		bool 	   reset;
		int  	   seq;

		float 		CMAXX, CMAXY, CMAXZ;
		float 		CMAXR, CMAXP, CMAXH;
		float 		CMINX, CMINY, CMINZ;
		float		CMINR, CMINP, CMINH;
		float 		CMAXVZ, CMINVZ;
		float 		CMAXDR, CMAXDP, CMAXDH;
		float 		CMINDR, CMINDP, CMINDH;

		float 	   CLASTTIME;
		float 	   CAMBUFF;
		float	   CAMMAXTIME;
		CameraVar();
		void GetCameraPosition(void);
		void ResetCameraVars(void);
	};

	class Aircraft
	{
		private:
			XPLMDataRef	dr_plane_x;
			XPLMDataRef	dr_plane_y;
			XPLMDataRef	dr_plane_z;
			XPLMDataRef	dr_plane_the;
			XPLMDataRef	dr_plane_phi;
			XPLMDataRef	dr_plane_psi;

			XPLMDataRef	dr_plane_velx;
			XPLMDataRef	dr_plane_vely;
			XPLMDataRef	dr_plane_velz;

		public:
			float		plane_x;
			float		plane_y;
			float		plane_z;
			float		plane_the;
			float		plane_phi;
			float		plane_psi;

			float		vel_x;
			float		vel_y;
			float		vel_z;

		Aircraft(int AircraftNo);
		void GetAircraftData(void);
		void SetAircraftData(void);
	};

	struct Weather {
		Weather(const std::vector<int>& cloud_layer,
			const std::vector<float>& cloud_base,
			const std::vector<float>& cloud_top,
			float vis,
			float rain_percent,
			float thunder_percent
		);
		Weather();
		void SetDrefs();
		static XPLMDataRef	    gcloudtype_0;
		static XPLMDataRef	    gcloudtype_1;
		static XPLMDataRef	    gcloudtype_2;
		static XPLMDataRef	    gcloudbase_0;
		static XPLMDataRef	    gcloudbase_1;
		static XPLMDataRef	    gcloudbase_2;
		static XPLMDataRef	    gcloudtops_0;
		static XPLMDataRef	    gcloudtops_1;
		static XPLMDataRef	    gcloudtops_2;
		static XPLMDataRef	    grainpercent;
		static XPLMDataRef	    gthunderpercent;
		static XPLMDataRef	    gvisibility;

		std::vector<int> ctype;
		std::vector<float> cbase;
		std::vector<float> ctop;

		float vis, rain, thunder;
	};

	struct EnvironmentVar {
		std::vector<float> time = { 6.4 * 60 * 60,  12 * 60 * 60, 15 * 60 * 60,  19.63 * 60 * 60 };
		std::vector<Weather> weather_preset;
		std::vector<Weather>::iterator weather_iterator;
		std::vector<float>::iterator time_iterator;
		float initial_x, initial_y, initial_z;
	};

	struct RecordingVar {
		const int max_mul_acf = 15;
		int capturing_state;

		cv::Mat img;
		float screen_w, screen_h;
		int seq_req = 50;
		int frame_per_seq = 50;

		float MINCTIME = 10.0;
		float MAXCTIME = 40.0;
	};

	struct DebugVars {
		bool exec_first = true;
	};

};
