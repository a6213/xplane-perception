/*
 * This file is part of {{ cookiecutter.package_name }}.
 *
 * Developed for the AIR Lab CMU, RI.
 * This product includes software developed by the Mitsubishi Project
 * (https://theairlab.org/).
 * See the COPYRIGHT file at the top-level directory of this distribution
 * for details of code ownership.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define GLEW_STATIC

#include <string.h>
#include <sstream>
#include <ctime>
#include <math.h>
#include <vector>
#include <algorithm>
#include <Eigen/Dense>
#include <gl/glew.h>

#include <opencv2/core/core.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <thread>

#include "XpDataGen/xp_data_gen.h"
#ifndef XPLM300
#error This is made to be compiled against the XPLM300 SDK
#endif

//Opengl32.lib

using namespace xpd;
/*
 Constants Initialized here
*/
const float M_PI          = 3.141592;
const float MIN_SAMP_Z    = 0500.0;
const float MAX_SAMP_Z    = 1800.0;
const float RADF          = M_PI / 180;
const float START_T       = 1.0; // Initial Delay time
const float D_REC_T       = 0.1; // Time After Pausing Data Should Be recorded
const float D_UNPAUSE_T   = 1.0; // Time after which simulation must be unpaused after data collection
const float D_PAUSE_T     = 0.1; // This is the frequency of Capturing data

const float TARGET_LAT = 47.463559;//37.412393//40.753411
const float TARGET_LON = -122.307754;//-122.993110//-73.
const float time_zone = -5.0;

const float KPCOL = 0.8;
const float KDCOL = 0.4;



int g_menu_container_idx; // The index of our menu item in the Plugins menu
XPLMMenuID g_menu_id; // The menu container we'll append all our menu items to


Weather::Weather() {};

Weather::Weather(const std::vector<int>& cloud_layer,
	const std::vector<float>& cloud_base,
	const std::vector<float>& cloud_top,
	float _vis,
	float _rain,
	float _thunder): ctype{cloud_layer}, cbase{cloud_base}, ctop{cloud_top}, vis{_vis}, rain{_rain}, thunder{_thunder}
{
}
void Weather::SetDrefs()
{
	XPLMSetDatai(gcloudtype_0, ctype[0]);
	XPLMSetDatai(gcloudtype_1, ctype[1]);
	XPLMSetDatai(gcloudtype_2, ctype[2]);
	XPLMSetDataf(gcloudbase_0, cbase[0]);
	XPLMSetDataf(gcloudbase_1, cbase[1]);
	XPLMSetDataf(gcloudbase_2, cbase[2]);
	XPLMSetDataf(gcloudtops_0, ctop[0]);
	XPLMSetDataf(gcloudtops_1, ctop[1]);
	XPLMSetDataf(gcloudtops_2, ctop[2]);
	XPLMSetDataf(grainpercent, rain);
	XPLMSetDataf(gthunderpercent, thunder);
	XPLMSetDataf(gvisibility, vis);
}


CameraVar::CameraVar() {
	init_time = 0.0;
	init = false;
	reset = true;
	seq = 0;

	CMAXX = 25000.0;
	CMAXY = 1000.0;
	CMAXZ = -30000.0;
	CMAXR = 10;
	CMAXP = 5.0;
	CMAXH = 360;

	CMINX = 15000.0;
	CMINY = 500.0;
	CMINZ = -20000.0;
	CMINR = -10.0;
	CMINP = 0.0;
	CMINH = 0;

	CMAXVZ = 75;
	CMINVZ = 30;

	CMAXDR = 0.5;
	CMINDR = -0.5;
	CMAXDP = 0.5;
	CMINDP = -0.5;
	CMAXDH = 1.0;
	CMINDH = -1.0;

	CLASTTIME = 0.0;
	CAMBUFF = 0.1;
	CAMMAXTIME = 25.0;
}

void CameraVar::ResetCameraVars(void)
{
	init_time = 0.0;
	init = false;
	reset = false;
	CLASTTIME = 0.0;
}

Aircraft::Aircraft(int AircraftNo)
{
	char	x_str[80];
	char	y_str[80];
	char	z_str[80];
	char	the_str[80];
	char	phi_str[80];
	char	psi_str[80];

	char	vx_str[80];
	char	vy_str[80];
	char	vz_str[80];

	sprintf(x_str, "sim/multiplayer/position/plane%d_x", AircraftNo);
	sprintf(y_str, "sim/multiplayer/position/plane%d_y", AircraftNo);
	sprintf(z_str, "sim/multiplayer/position/plane%d_z", AircraftNo);
	sprintf(the_str, "sim/multiplayer/position/plane%d_the", AircraftNo);
	sprintf(phi_str, "sim/multiplayer/position/plane%d_phi", AircraftNo);
	sprintf(psi_str, "sim/multiplayer/position/plane%d_psi", AircraftNo);
	sprintf(vx_str, "sim/multiplayer/position/plane%d_v_x", AircraftNo);
	sprintf(vy_str, "sim/multiplayer/position/plane%d_v_y", AircraftNo);
	sprintf(vz_str, "sim/multiplayer/position/plane%d_v_z", AircraftNo);

	dr_plane_x = XPLMFindDataRef(x_str);
	dr_plane_y = XPLMFindDataRef(y_str);
	dr_plane_z = XPLMFindDataRef(z_str);
	dr_plane_the = XPLMFindDataRef(the_str);
	dr_plane_phi = XPLMFindDataRef(phi_str);
	dr_plane_psi = XPLMFindDataRef(psi_str);
	dr_plane_velx = XPLMFindDataRef(vx_str);
	dr_plane_vely = XPLMFindDataRef(vy_str);
	dr_plane_velz = XPLMFindDataRef(vz_str);

}

void Aircraft::GetAircraftData(void)
{

	plane_x = XPLMGetDataf(dr_plane_x);
	plane_y = XPLMGetDataf(dr_plane_y);
	plane_z = XPLMGetDataf(dr_plane_z);

	plane_the = XPLMGetDataf(dr_plane_the);
	plane_phi = XPLMGetDataf(dr_plane_phi);
	plane_psi = XPLMGetDataf(dr_plane_psi);

	vel_x = XPLMGetDataf(dr_plane_velx);
	vel_y = XPLMGetDataf(dr_plane_vely);
	vel_z = XPLMGetDataf(dr_plane_velz);
}

void Aircraft::SetAircraftData(void)
{
	XPLMSetDataf(dr_plane_x, plane_x);
	XPLMSetDataf(dr_plane_y, plane_y);
	XPLMSetDataf(dr_plane_z, plane_z);
	XPLMSetDataf(dr_plane_the, plane_the);
	XPLMSetDataf(dr_plane_phi, plane_phi);
	XPLMSetDataf(dr_plane_psi, plane_psi);
}

std::vector<Aircraft> mpacf;

std::vector<int> acf_ind_shuffler;
std::vector<int> acfs_on_screen;

std::vector<double> coll_x, coll_y, coll_z, coll_vx, coll_vy, coll_vz, coll_ax, coll_ay, coll_az;

// Transform matrices - we will use these to figure out where we shuold should have drawn.
static XPLMDataRef	    s_matrix_wrl = XPLMFindDataRef("sim/graphics/view/world_matrix");
static XPLMDataRef	    s_matrix_proj = XPLMFindDataRef("sim/graphics/view/projection_matrix_3d");
static XPLMDataRef	    screen_width = XPLMFindDataRef("sim/graphics/view/window_width");
static XPLMDataRef	    screen_height = XPLMFindDataRef("sim/graphics/view/window_height");

static XPLMDataRef		gcamX = XPLMFindDataRef("sim/graphics/view/view_x");
static XPLMDataRef		gcamY = XPLMFindDataRef("sim/graphics/view/view_y");
static XPLMDataRef		gcamZ = XPLMFindDataRef("sim/graphics/view/view_z");
static XPLMDataRef		gcamRoll = XPLMFindDataRef("sim/graphics/view/view_roll");
static XPLMDataRef		gcamPitch = XPLMFindDataRef("sim/graphics/view/view_pitch");
static XPLMDataRef		gcamHead = XPLMFindDataRef("sim/graphics/view/view_heading");

static XPLMDataRef		gPlaneLat = XPLMFindDataRef("sim/flightmodel/position/latitude");
static XPLMDataRef		gPlaneLong = XPLMFindDataRef("sim/flightmodel/position/longitude");
static XPLMDataRef		gPlaneX = XPLMFindDataRef("sim/flightmodel/position/local_x");
static XPLMDataRef		gPlaneY = XPLMFindDataRef("sim/flightmodel/position/local_y");
static XPLMDataRef		gPlaneZ = XPLMFindDataRef("sim/flightmodel/position/local_z");
static XPLMDataRef		gPlaneVX = XPLMFindDataRef("sim/flightmodel/position/local_vx");
static XPLMDataRef		gPlaneVY = XPLMFindDataRef("sim/flightmodel/position/local_vy");
static XPLMDataRef		gPlaneVZ = XPLMFindDataRef("sim/flightmodel/position/local_vz");
static XPLMDataRef		gPlaneAX = XPLMFindDataRef("sim/flightmodel/position/local_ax");
static XPLMDataRef		gPlaneAY = XPLMFindDataRef("sim/flightmodel/position/local_ay");
static XPLMDataRef		gPlaneAZ = XPLMFindDataRef("sim/flightmodel/position/local_az");
static XPLMDataRef		gPlaneTheta = XPLMFindDataRef("sim/flightmodel/position/theta");
static XPLMDataRef		gPlanePhi = XPLMFindDataRef("sim/flightmodel/position/phi");
static XPLMDataRef		gPlanePsi = XPLMFindDataRef("sim/flightmodel/position/psi");
static XPLMDataRef		ov_plane_path = XPLMFindDataRef("sim/operation/override/override_planepath");
static XPLMDataRef		gApHead = XPLMFindDataRef("sim/cockpit/autopilot/heading");
static XPLMDataRef		gApVerticalVel = XPLMFindDataRef("sim/cockpit/autopilot/vertical_velocity");


static XPLMDataRef		gvisiblity = XPLMFindDataRef("sim/weather/visibility_reported_m");
static XPLMDataRef		grain_percent = XPLMFindDataRef("sim/weather/rain_percent");
static XPLMDataRef      gzulu_time = XPLMFindDataRef("sim/time/zulu_time_sec");
static XPLMDataRef      glocal_time = XPLMFindDataRef("sim/time/local_time_sec");

static XPLMCommandRef   screenshot = XPLMFindCommand("sim/operation/screenshot");
static XPLMCommandRef   pause_sim = XPLMFindCommand("sim/operation/pause_toggle");
static XPLMDataRef      sim_paused = XPLMFindDataRef("sim/time/paused");
static XPLMDataRef      frame_period = XPLMFindDataRef("sim/time/framerate_period");
static XPLMDataRef      fov_v = XPLMFindDataRef("sim/graphics/view/vertical_field_of_view_deg");
static XPLMDataRef      fov_h = XPLMFindDataRef("sim/graphics/view/field_of_view_deg");

static XPLMDataRef      ov_prop_disc = XPLMFindDataRef("sim/flightmodel2/engines/prop_disc/override");
static XPLMDataRef      gPropIsDisc = XPLMFindDataRef("sim/flightmodel2/engines/prop_is_disc");
static XPLMDataRef      gPropRot = XPLMFindDataRef("sim/flightmodel2/engines/prop_rotation_angle_deg");


XPLMDataRef Weather::gcloudtype_0 = XPLMFindDataRef("sim/weather/cloud_type[0]");
XPLMDataRef Weather::gcloudtype_1 = XPLMFindDataRef("sim/weather/cloud_type[1]");
XPLMDataRef Weather::gcloudtype_2 = XPLMFindDataRef("sim/weather/cloud_type[2]");
XPLMDataRef Weather::gcloudbase_0 = XPLMFindDataRef("sim/weather/cloud_base_msl_m[0]");
XPLMDataRef Weather::gcloudbase_1 = XPLMFindDataRef("sim/weather/cloud_base_msl_m[1]");
XPLMDataRef Weather::gcloudbase_2 = XPLMFindDataRef("sim/weather/cloud_base_msl_m[2]");
XPLMDataRef Weather::gcloudtops_0 = XPLMFindDataRef("sim/weather/cloud_tops_msl_m[0]");
XPLMDataRef Weather::gcloudtops_1 = XPLMFindDataRef("sim/weather/cloud_tops_msl_m[1]");
XPLMDataRef Weather::gcloudtops_2 = XPLMFindDataRef("sim/weather/cloud_tops_msl_m[2]");
XPLMDataRef Weather::grainpercent = XPLMFindDataRef("sim/weather/rain_percent");
XPLMDataRef Weather::gthunderpercent = XPLMFindDataRef("sim/weather/thunderstorm_percent");
XPLMDataRef Weather::gvisibility = XPLMFindDataRef("sim/weather/visibility_reported_m");
/*For Data logging*/
FILE* g_op_file;
char outputPath[255];
char op_f_name[255];
char temp_name[100];

/*File names and timeing purpose*/
int global_image_count = 0;
time_t now;
struct tm* l_now;

std::stringstream DEBUGACF;
float final_x, final_y;

CameraVar cam_main;
CameraVar* cam_ptr = nullptr;

RecordingVar rec_main;
RecordingVar* rec_var_ptr;

DebugVars deb_var;
EnvironmentVar env_var;
EnvironmentVar* env_var_ptr;

void CameraVar::GetCameraPosition(void)
{
	x = XPLMGetDataf(gcamX);
	y = XPLMGetDataf(gcamY);
	z = XPLMGetDataf(gcamZ);
	roll = XPLMGetDataf(gcamRoll);
	pitch = XPLMGetDataf(gcamPitch);
	head = XPLMGetDataf(gcamHead);
	init = true;
	init_time = XPLMGetElapsedTime();
}

void SetCameraPtr()
{
	double cx, cy, cz, lat, lon, alt;
	lat = XPLMGetDataf(gPlaneLat);
	lon = XPLMGetDataf(gPlaneLong);
	alt = 200.0;

	XPLMWorldToLocal(lat, lon, alt, &cx, &cy, &cz);
	cam_ptr->CMAXX = cx + 5000.0;
	cam_ptr->CMAXZ = cz + 5000.0;
	cam_ptr->CMINX = cx - 5000.0;
	cam_ptr->CMINZ = cz - 5000.0;

	XPLMDebugString("Target position: ");
	DEBUGACF << cx << ", " << cy << ", " << cz << "|| " << TARGET_LAT << ", " << TARGET_LON << "\n"; // << cam_ptr -> x << "," << cam_ptr -> y << "," << cam_ptr -> z << "," << mpacf[i].plane_x << "," << mpacf[i].plane_y << "," << mpacf[i].plane_z << "\n";
	XPLMDebugString((char*)DEBUGACF.str().c_str());
	DEBUGACF.str("");
}

void RecordData(const std::string& f_name)
{
	/* Write the data to a file. */
	g_op_file = fopen(f_name.c_str(), "w");
	
	/*Read the model view and projection matrices from this frame*/
	float mv[16], proj[16], vis, rain, thunder, zulu_time, local_time;
	int cloud_type_0;
	double cam_lat, cam_long, cam_alt;
	char raw_file_data[15000];

	XPLMGetDatavf(s_matrix_wrl, mv, 0, 16);
	XPLMGetDatavf(s_matrix_proj, proj, 0, 16);
	XPLMLocalToWorld(cam_ptr->x, cam_ptr->y, cam_ptr->z, &cam_lat, &cam_long, &cam_alt);

	vis = XPLMGetDataf(Weather::gvisibility);
	rain = XPLMGetDataf(Weather::grainpercent);
	thunder = XPLMGetDataf(Weather::gthunderpercent);
	zulu_time = XPLMGetDataf(gzulu_time);
	local_time = XPLMGetDataf(glocal_time);
	cloud_type_0 = XPLMGetDatai(Weather::gcloudtype_0);
	
	if (rec_var_ptr->capturing_state == USRS || rec_var_ptr->capturing_state == USRM || rec_var_ptr->capturing_state == USRC)
	{
		char f_name[256], f_path[512];
		XPLMGetNthAircraftModel(0,
			f_name,
			f_path);

		float user_x, user_y, user_z, user_th, user_ph, user_ps, user_vx, user_vy, user_vz;
		user_x = XPLMGetDataf(gPlaneX);
		user_y = XPLMGetDataf(gPlaneY);
		user_z = XPLMGetDataf(gPlaneZ);
		user_th = XPLMGetDataf(gPlaneTheta);
		user_ph = XPLMGetDataf(gPlanePhi);
		user_ps = XPLMGetDataf(gPlanePsi);
		user_vx = XPLMGetDataf(gPlaneVX);
		user_vy = XPLMGetDataf(gPlaneVY);
		user_vz = XPLMGetDataf(gPlaneVZ);
		sprintf(raw_file_data, "%f %f %f %f %f %f %f %f %f\t",
			user_x, user_y, user_z,
			user_th, user_ph, user_ps,
			user_vx, user_vy, user_vz);
		sprintf(raw_file_data + strlen(raw_file_data), f_name);
		sprintf(raw_file_data + strlen(raw_file_data), "\n");

		for (auto it = acfs_on_screen.begin(); it != acfs_on_screen.end(); it++)
		{
			if (rec_var_ptr->capturing_state == USRS || rec_var_ptr->capturing_state == USRC) break;
			char f_name[256], f_path[512];

			mpacf[*it].GetAircraftData();
			XPLMGetNthAircraftModel(*it + 1,
				f_name,
				f_path);

			sprintf(raw_file_data +  strlen(raw_file_data), "%f %f %f %f %f %f %f %f %f ",
				mpacf[*it].plane_x, mpacf[*it].plane_y, mpacf[*it].plane_z,
				mpacf[*it].plane_the, mpacf[*it].plane_phi, mpacf[*it].plane_psi,
				mpacf[*it].vel_x, mpacf[*it].vel_y, mpacf[*it].vel_z);

			sprintf(raw_file_data + strlen(raw_file_data), f_name);
			sprintf(raw_file_data + strlen(raw_file_data), "\n");
		}
	}
	else {
		XPLMDebugString("Recording Acf Metadata...\n");
		for (auto it = acfs_on_screen.begin(); it != acfs_on_screen.end(); ++it)
		{
			XPLMDebugString("Recorded one acf..\n");
			char f_name[256], f_path[512];

			mpacf[*it].GetAircraftData();
			XPLMGetNthAircraftModel(*it + 1,
				f_name,
				f_path);
			if (it == acfs_on_screen.begin()) {
				sprintf(raw_file_data, "%f %f %f %f %f %f %f %f %f ",
					mpacf[*it].plane_x, mpacf[*it].plane_y, mpacf[*it].plane_z,
					mpacf[*it].plane_the, mpacf[*it].plane_phi, mpacf[*it].plane_psi,
					mpacf[*it].vel_x, mpacf[*it].vel_y, mpacf[*it].vel_z);
			}
			else
			{
				sprintf(raw_file_data + strlen(raw_file_data), "%f %f %f %f %f %f %f %f %f ",
					mpacf[*it].plane_x, mpacf[*it].plane_y, mpacf[*it].plane_z,
					mpacf[*it].plane_the, mpacf[*it].plane_phi, mpacf[*it].plane_psi,
					mpacf[*it].vel_x, mpacf[*it].vel_y, mpacf[*it].vel_z);
			}

			sprintf(raw_file_data + strlen(raw_file_data), f_name);
			sprintf(raw_file_data + strlen(raw_file_data), "\n");

		}

	}
	
	for (int i = 0; i < 16; ++i)
	{
		sprintf(raw_file_data + strlen(raw_file_data), "%f ", mv[i]);
	}
	sprintf(raw_file_data + strlen(raw_file_data), "\n");

	for (int i = 0; i < 16; ++i)
	{
		sprintf(raw_file_data + strlen(raw_file_data), "%f ", proj[i]);
	}
	sprintf(raw_file_data + strlen(raw_file_data), "\n");

	sprintf(raw_file_data + strlen(raw_file_data), "%f %f %f\n", cam_lat, cam_long, cam_alt);
	sprintf(raw_file_data + strlen(raw_file_data), "%f %f %f %d %f %f\n",
		vis, rain, thunder,
		cloud_type_0, zulu_time, local_time);
	sprintf(raw_file_data + strlen(raw_file_data), "%d\n", cam_ptr->seq);
	fputs(raw_file_data, g_op_file);
	fclose(g_op_file);
}

static int CaptureScreenCb(XPLMDrawingPhase inPhase, int inIsBefore, void* inRefcon) {
	glReadBuffer(GL_BACK);
	////use fast 4-byte alignment (default anyway) if possible
	glPixelStorei(GL_PACK_ALIGNMENT, (rec_var_ptr->img.step & 3) ? 1 : 4);
	glReadPixels(0, 0, rec_var_ptr->img.cols, rec_var_ptr->img.rows, GL_BGR_EXT, GL_UNSIGNED_BYTE, rec_var_ptr->img.data);

	if (CaminitTimeDelta(0.25)) {
		CheckOutAcfs();
		return 1;
	}

	else if (cam_ptr->init && !cam_ptr->reset) {

		std::string name = "dataset\\" + std::to_string(global_image_count) + ".bmp";
		std::string raw_name = "dataset\\" + std::to_string(global_image_count) + ".raw";
		RecordData(raw_name);
		cv::imwrite(name, rec_var_ptr->img);
		global_image_count++;
	}
	return 1;
}

float InitStartupVar(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{
	SetCameraPtr();
	XPLMControlCamera(xplm_ControlCameraForever, CameraControl, NULL);

	XPLMRegisterFlightLoopCallback(PauseSimRec, START_T, NULL);
	XPLMRegisterFlightLoopCallback(RecordDataCallback, 0.0, NULL);
	XPLMRegisterFlightLoopCallback(UnPauseSimRec, 0.0, NULL);

	return 0.0;
}


float ConfigureEnv(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{
	XPLMDebugString("Configuring Environments...\n");
	XPLMUnregisterDrawCallback(CaptureScreenCb, xplm_Phase_LastCockpit, 0, NULL);
	XPLMDontControlCamera();
	cam_ptr->ResetCameraVars();

	if (rec_var_ptr->capturing_state == USRC) {
		if (cam_ptr->seq > rec_var_ptr->seq_req) {
			XPLMRegisterFlightLoopCallback(DestroyRecorders, -1, NULL);
		}
		else {
			XPLMRegisterFlightLoopCallback(CreateRecorders, 2.0, NULL);
		}
		return 0.0;
	}

	if (rec_var_ptr->capturing_state == OWNC) {
		if (cam_ptr->seq > rec_var_ptr->seq_req) {
			XPLMRegisterFlightLoopCallback(DestroyRecorders, -1, NULL);
		}
		else {
			XPLMRegisterFlightLoopCallback(CreateRecorders, 2.0, NULL);
		}
		return 0.0;
	}

	if (env_var_ptr->weather_iterator != env_var_ptr->weather_preset.end() || env_var_ptr->time_iterator != env_var_ptr->time.end())
	{
		if (env_var_ptr->time_iterator != env_var_ptr->time.end()) {
			XPLMSetDataf(gzulu_time, *(env_var_ptr->time_iterator) - time_zone*60*60);
			XPLMSetDataf(gPlaneX, env_var_ptr->initial_x);
			XPLMSetDataf(gPlaneY, env_var_ptr->initial_y);
			XPLMSetDataf(gPlaneZ, env_var_ptr->initial_z);
			XPLMRegisterFlightLoopCallback(CreateRecorders, 2.0, NULL);
			env_var_ptr->time_iterator++;
		}

		else {
			env_var_ptr->time_iterator = env_var_ptr->time.begin();
			XPLMSetDataf(gPlaneX, env_var_ptr->initial_x);
			XPLMSetDataf(gPlaneY, env_var_ptr->initial_y);
			XPLMSetDataf(gPlaneZ, env_var_ptr->initial_z);

			XPLMSetDataf(gzulu_time, *(env_var_ptr->time_iterator) - time_zone * 60 * 60);
			env_var_ptr->time_iterator++;

			float max_angle = fmax(abs(cam_ptr->CMINP), abs(cam_ptr->CMAXP));
			float min_cloud_base = 1.0 * abs(XPLMGetDataf(gPlaneY) + MAX_SAMP_Z * sin(fmin(M_PI/2, max_angle*RADF + M_PI/5)));

			for (int i = 0; i < 3; ++i) {
				env_var_ptr->weather_iterator->cbase[i] = min_cloud_base;
				env_var_ptr->weather_iterator->ctop[i] = min_cloud_base + 1000.0;
			}
			env_var_ptr->weather_iterator->SetDrefs();
			XPLMRegisterFlightLoopCallback(CreateRecorders, 2.0, NULL);
			env_var_ptr->weather_iterator++;
		}

		if (env_var_ptr->weather_iterator == env_var_ptr->weather_preset.begin()) {

			float max_angle = fmax(abs(cam_ptr->CMINP), abs(cam_ptr->CMAXP));
			float min_cloud_base = 1.0 * abs(XPLMGetDataf(gPlaneY) + MAX_SAMP_Z );

			for (int i = 0; i < 3; ++i) {
				env_var_ptr->weather_iterator->cbase[i] = min_cloud_base;
				env_var_ptr->weather_iterator->ctop[i] = min_cloud_base + 1000.0;
			}
			env_var_ptr->weather_iterator->SetDrefs();
			XPLMRegisterFlightLoopCallback(CreateRecorders, 2.0, NULL);
			env_var_ptr->weather_iterator++;

		}
	}

	else {
		XPLMRegisterFlightLoopCallback(DestroyRecorders, -1, NULL);
	}

	return 0.0;
}

float CreateRecorders(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{
	if (rec_var_ptr->capturing_state == OWNC) {
		XPLMRegisterFlightLoopCallback(OwnshipCollider, -1, NULL);
		XPLMRegisterDrawCallback(CaptureScreenCb, xplm_Phase_LastCockpit, 0, NULL);
		return 0.0;
	}
	XPLMControlCamera(xplm_ControlCameraForever, CameraControl, NULL);
	XPLMRegisterDrawCallback(CaptureScreenCb, xplm_Phase_LastCockpit, 0, NULL);
	return 0.0;
}

float DestroyRecorders(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{
	XPLMUnregisterDrawCallback(CaptureScreenCb, xplm_Phase_LastCockpit, 0, NULL);
	XPLMDontControlCamera();
	XPLMUnregisterFlightLoopCallback(ConfigureEnv, NULL);
	XPLMUnregisterFlightLoopCallback(CreateRecorders, NULL);
	XPLMUnregisterFlightLoopCallback(DestroyRecorders, NULL);
	XPLMUnregisterFlightLoopCallback(OwnshipCollider, NULL);
	return 0.0;
}

float	RecordDataCallback(
	float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{

	/*Getting the present time to save as the filename*/
	time(&now);
	l_now = localtime(&now);

	/*setting the filename*/
	op_f_name[0] = 0;
	strcpy(op_f_name, outputPath);

	sprintf(temp_name, "%04d-%02d-%02d %02d.%02d.%02d.raw", l_now->tm_year + 1900, l_now->tm_mon + 1, l_now->tm_mday, l_now->tm_hour, l_now->tm_min, l_now->tm_sec);
	strcat(op_f_name, temp_name);
	temp_name[0] = 0;

	/* Send Command to take SS */
	XPLMCommandOnce(screenshot);
	RecordData(op_f_name);

	XPLMSetFlightLoopCallbackInterval(UnPauseSimRec, D_UNPAUSE_T, 1, NULL);
	return 0.0;
}

bool CaminitTimeDelta(float t)
{
	float curr_time = XPLMGetElapsedTime();
	return (curr_time - cam_ptr->init_time < t || !cam_ptr->init);
}

float PauseSimRec(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{
	//cam_ptr->init = false;
	if (CaminitTimeDelta(0.35)) {
		return 1.0;
	}
	XPLMCommandOnce(pause_sim);
	XPLMSetFlightLoopCallbackInterval(RecordDataCallback, D_REC_T, 1, NULL);
	return 0.0;
}

float UnPauseSimRec(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{	
	if (rec_var_ptr->capturing_state == NONSEQ)
	{
		cam_ptr->init = false;
	}
	XPLMCommandOnce(pause_sim);
	XPLMSetFlightLoopCallbackInterval(PauseSimRec, D_PAUSE_T, 1, NULL);
	return 0.0;
}

void SetPresetWeather()
{
	env_var_ptr->time = { 12 * 60 * 60 };
	env_var_ptr->weather_preset.push_back(
		Weather({ 0,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 40233.6, 0.0, 0.0)
	);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 1,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 40233.6, 0.0, 0.0)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 2,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 40233.6, 0.0, 0.0)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 3,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 32186.8, 0.1, 0.0)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 3,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 20140.6, 0.3, 0.2)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 4,4,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 8046.7, 0.25, 0.0)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 4,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 24140.6, 0.75, 0.6)
	//);
	//env_var_ptr->weather_preset.push_back(
	//	Weather({ 4,0,0 }, { 3000.0, 5000.0, 5000.0 }, { 5000.0,8000.0,8000.0 }, 24140.6, 0.9, 0.8)
	//);

	env_var_ptr->weather_iterator = env_var_ptr->weather_preset.begin();
	env_var_ptr->time_iterator = env_var_ptr->time.begin();
}

PLUGIN_API int XPluginStart(char* outName, char* outSig, char* outDesc)
{
	srand(time(NULL));
	strcpy(outName, "DataGen");
	strcpy(outSig, "com.airlab.datagen");
	strcpy(outDesc, "A plugin to generate dataset for AI Airforce task");

	cam_ptr = &cam_main;
	rec_var_ptr = &rec_main;
	env_var_ptr = &env_var;
	SetPresetWeather();

	final_x = 0.0;
	final_y = 0.0;
	acfs_on_screen = {};

	for (int i = 1; i <= rec_var_ptr->max_mul_acf; i++) {
		mpacf.push_back(Aircraft(i));
	}

	XPLMGetSystemPath(outputPath);
	strcat(outputPath, "dataset/");

	rec_var_ptr->screen_w = XPLMGetDatai(screen_width);
	rec_var_ptr->screen_h = XPLMGetDatai(screen_height);
	rec_var_ptr->img.create(rec_var_ptr->screen_h, rec_var_ptr->screen_w, CV_8UC3);

	g_menu_container_idx = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "Data Gen", 0, 0);
	g_menu_id = XPLMCreateMenu("Data Gen", XPLMFindPluginsMenu(), g_menu_container_idx, MenuHandler, NULL);
	XPLMAppendMenuItem(g_menu_id, "Non Seq", (void*)"Menu Item 1", 1);
	XPLMAppendMenuItem(g_menu_id, "Seq Cam", (void*)"Menu Item 2", 1);
	XPLMAppendMenuItem(g_menu_id, "Seq Acf", (void*)"Menu Item 3", 1);
	XPLMAppendMenuItem(g_menu_id, "User Multi", (void*)"Menu Item 4", 1);
	XPLMAppendMenuItem(g_menu_id, "User Single", (void*)"Menu Item 5", 1);
	XPLMAppendMenuItem(g_menu_id, "User Collision", (void*)"Menu Item 6", 1);
	XPLMAppendMenuItem(g_menu_id, "Cockpit", (void*)"Menu Item 7", 1);
	return 1;
}


float GetRandomRange(float min, float max)
{
	float num = min + ((float)rand() / (float)RAND_MAX) * (max - min);
	return num;
}

float GetExpRange(float min, float max, float min_exp = 0.0, float max_exp = 4.0)
{
	float num = min + exp(-1 * GetRandomRange(min_exp, max_exp)) * (max - min);
	return num;
}

void ResetCamera(){

	cam_ptr->x = GetRandomRange(cam_ptr->CMINX, cam_ptr->CMAXX);
	cam_ptr->y = GetRandomRange(cam_ptr->CMINY, cam_ptr->CMAXY);
	cam_ptr->z = GetRandomRange(cam_ptr->CMINZ, cam_ptr->CMAXZ);
	
	cam_ptr->roll = GetRandomRange(cam_ptr->CMINR, cam_ptr->CMAXR);
	cam_ptr->pitch = GetRandomRange(cam_ptr->CMINP, cam_ptr->CMAXP);
	cam_ptr->head = GetRandomRange(cam_ptr->CMINH, cam_ptr->CMAXH);
	
	cam_ptr->vz = GetRandomRange(cam_ptr->CMINVZ, cam_ptr->CMAXVZ);
	cam_ptr->droll = GetRandomRange(cam_ptr->CMINDR, cam_ptr->CMAXDR);
	cam_ptr->dpitch = GetRandomRange(cam_ptr->CMINDP, cam_ptr->CMAXDP);
	cam_ptr->dhead = GetRandomRange(cam_ptr->CMINDH, cam_ptr->CMAXDH);

	cam_ptr->init = true;
	cam_ptr->reset = false;

	cam_ptr->init_time = XPLMGetElapsedTime();
	cam_ptr->seq = cam_ptr->seq + 1;
	cam_ptr->CLASTTIME = XPLMGetElapsedTime();
}

void ResetCameraAroundUser()
{
	float user_x, user_y, user_z , user_vx, user_vy, user_vz;
	XPLMSetDataf(gPlaneY, env_var_ptr->initial_y);
	user_x = XPLMGetDataf(gPlaneX);
	user_y = XPLMGetDataf(gPlaneY);
	user_z = XPLMGetDataf(gPlaneZ);
	user_vx = XPLMGetDataf(gPlaneVX);
	user_vy = XPLMGetDataf(gPlaneVY);
	user_vz = XPLMGetDataf(gPlaneVZ);

	if (rec_var_ptr->capturing_state == USRC) {
		float time_to_coll = GetRandomRange(rec_var_ptr->MINCTIME, rec_var_ptr->MAXCTIME);
		float goal_x, goal_y, goal_z;

		goal_x = user_x + user_vx * time_to_coll;
		goal_y = user_y + user_vy * time_to_coll;
		goal_z = user_z + user_vz * time_to_coll;

		float cam_pitch = GetRandomRange(-cam_ptr->CMAXP, -cam_ptr->CMINP);
		float theta_user = atan2(goal_z - user_z, goal_x - user_x);
		float theta_ref = GetRandomRange(theta_user - M_PI / 6, theta_user + M_PI / 6);

		float cam_x = cos(cam_pitch * RADF) * cos(theta_ref);
		float cam_z = cos(cam_pitch * RADF) * sin(theta_ref);
		float cam_y = sin(cam_pitch * RADF);

		cam_ptr->vz = GetRandomRange(cam_ptr->CMINVZ, cam_ptr->CMAXVZ);
		float mag = cam_ptr->vz * time_to_coll;
		cam_ptr->x = goal_x + mag * cam_x;
		cam_ptr->y = fmax(200.0, goal_y + mag * cam_y);
		cam_ptr->z = goal_z + mag * cam_z;

		cam_ptr->cvx = mag * cam_x / time_to_coll;
		cam_ptr->cvy = mag * cam_y / time_to_coll;
		cam_ptr->cvz = mag * cam_z / time_to_coll;


		cam_ptr->roll = 0.0;
		cam_ptr->pitch = -cam_pitch;
		float ang_x = atan2(goal_z - cam_ptr->z, goal_x - cam_ptr->x);
		cam_ptr->head = M_PI / 2 + ang_x > 0 ? M_PI / 2 + ang_x : 5 * M_PI / 2 + ang_x;
		cam_ptr->head *= 180.0 / M_PI;

		cam_ptr->droll  = 0.0;
		cam_ptr->dpitch = 0.0;
		cam_ptr->dhead  = 0.0;

		cam_ptr->CAMMAXTIME = time_to_coll;
	}
	else if (rec_var_ptr->capturing_state == OWNC) {}

	else {
		float cam_pitch = GetRandomRange(-cam_ptr->CMAXP, -cam_ptr->CMINP);
		float theta_ref = GetRandomRange(0, 2 * M_PI);
		float cam_x = cos(cam_pitch * RADF) * cos(theta_ref + M_PI / 2);
		float cam_z = cos(cam_pitch * RADF) * sin(theta_ref + M_PI / 2);
		float cam_y = sin(cam_pitch * RADF);

		float mag = MIN_SAMP_Z + (float)(cam_ptr->seq % rec_var_ptr->seq_req) / (float)rec_var_ptr->seq_req * (MAX_SAMP_Z - MIN_SAMP_Z);

		cam_ptr->x = user_x + mag * cam_x;
		cam_ptr->y = fmax(200.0, user_y + mag * cam_y);
		cam_ptr->z = user_z + mag * cam_z;

		float dr = GetRandomRange(cam_ptr->CMINR, cam_ptr->CMAXR);
		float dp = GetRandomRange(-XPLMGetDataf(fov_v) / 4, XPLMGetDataf(fov_v) / 4);
		float dh = GetRandomRange(-XPLMGetDataf(fov_h) / 4, XPLMGetDataf(fov_h) / 4);

		cam_ptr->roll = dr;
		cam_ptr->pitch = -cam_pitch + dp;
		float ang_x = atan2(user_z - cam_ptr->z, user_x - cam_ptr->x);
		cam_ptr->head = fmod(2 * M_PI + M_PI / 2 + ang_x, 2 * M_PI) * 180.0 / M_PI + dh;

		cam_ptr->vz = GetRandomRange(cam_ptr->CMINVZ, cam_ptr->CMAXVZ);
		cam_ptr->droll = GetRandomRange(cam_ptr->CMINDR, cam_ptr->CMAXDR);
		cam_ptr->dpitch = GetRandomRange(cam_ptr->CMINDP, cam_ptr->CMAXDP);
		cam_ptr->dhead = GetRandomRange(cam_ptr->CMINDH, cam_ptr->CMAXDH);
	}
	
	cam_ptr->init = true;
	cam_ptr->reset = false;

	cam_ptr->init_time = XPLMGetElapsedTime();
	cam_ptr->seq = cam_ptr->seq + 1;
	cam_ptr->CLASTTIME = XPLMGetElapsedTime();

}

void CamframeToGlobal(float x, float y, float z, Aircraft* acf)
{
	float x_phi, y_phi, z_phi, x_the, y_the, z_the, x_wrl, y_wrl, z_wrl;
	x_phi = x * cos(cam_ptr->roll * RADF) + y * sin(cam_ptr->roll * RADF);
	y_phi = y * cos(cam_ptr->roll * RADF) - x * sin(cam_ptr->roll * RADF);
	z_phi = z;

	x_the = x_phi;
	y_the = y_phi * cos(cam_ptr->pitch * RADF) - z_phi * sin(cam_ptr->pitch * RADF);
	z_the = z_phi * cos(cam_ptr->pitch * RADF) + y_phi * sin(cam_ptr->pitch * RADF);

	x_wrl = x_the * cos(cam_ptr->head * RADF) - z_the * sin(cam_ptr->head * RADF) + cam_ptr->x;
	y_wrl = y_the + cam_ptr->y;
	z_wrl = z_the * cos(cam_ptr->head * RADF) + x_the * sin(cam_ptr->head * RADF) + cam_ptr->z;

	/*float mv[16];
	XPLMGetDatavf(s_matrix_wrl, mv, 0, 16);

	Eigen::Matrix<float, 4, 4, Eigen::ColMajor> Mview(mv);
	auto Mviewinv = Mview.inverse();
	Eigen::Vector4f next_cam_pos_local(x, y, z, 1.0);
	Eigen::Vector4f next_cam_pos_global = Mviewinv * next_cam_pos_local;*/


	acf->plane_x = x_wrl;//next_cam_pos_global[0];
	acf->plane_y = y_wrl;//fmax(200.0, next_cam_pos_global[1]);
	acf->plane_z = z_wrl;//next_cam_pos_global[2];
	acf->plane_the = 0.0;// GetRandomRange(cam_ptr->CMINR, cam_ptr->CMAXR);
	acf->plane_phi = 0.0;// GetRandomRange(cam_ptr->CMINP, cam_ptr->CMAXP);
	acf->plane_psi = 0.0;// GetRandomRange(cam_ptr->CMINH, cam_ptr->CMAXH);

}


void ResetAcfs()
{
	/*Assuming all the Alive acfs are mp and under ai control*/
	int tot_acf, tot_alive_acf;

	XPLMCountAircraft(&tot_acf, &tot_alive_acf, NULL);
	tot_alive_acf--;
	float _x_c, _y_c, _z_c;
	float _hfov = XPLMGetDataf(fov_h);
	float _vfov = XPLMGetDataf(fov_v);
	float _xmax, _ymax;
	acfs_on_screen.clear();
	acf_ind_shuffler.clear();

	for (int i = 0; i < rec_var_ptr->max_mul_acf; i++) {
		acf_ind_shuffler.push_back(i);
	}
	std::random_shuffle(&acf_ind_shuffler[0], &acf_ind_shuffler[tot_alive_acf]);

	int to_spawn = fmin(tot_alive_acf, 1 + rand() % 3);
	
	
	for (int i = 0; i < to_spawn; ++i)
	{
		_z_c = -GetRandomRange(MIN_SAMP_Z, MAX_SAMP_Z);
		if (rec_var_ptr->capturing_state != OWNC) {
			_xmax = abs(_z_c) * tan(_hfov / 4 * M_PI / 180);
			_ymax = abs(_z_c) * tan(_vfov / 4 * M_PI / 180);
			_x_c = _xmax* GetRandomRange(-1.0, 1.0);
			_y_c = _ymax* GetRandomRange(0.0, 1.0); //GetExpRange( -1.0, 0.0, 0.0, 5.0);

		}
		else {
			_xmax = abs(_z_c) * tan(_hfov / 4 * M_PI / 180);
			_ymax = abs(_z_c) * tan(_vfov / 4 * M_PI / 180);
			_x_c = _xmax * GetRandomRange(-1.0, 1.0);
			_y_c = _ymax * GetRandomRange(0.0, 1.0); //GetExpRange( -1.0, 0.0, 0.0, 5.0);
		}

		CamframeToGlobal(_x_c, _y_c, _z_c, &mpacf[acf_ind_shuffler[i]]);
		mpacf[acf_ind_shuffler[i]].SetAircraftData();
		acfs_on_screen.push_back(acf_ind_shuffler[i]);
	}

	for (int i = 0; i < tot_alive_acf; ++i)
	{
		if (std::find(acfs_on_screen.begin(), acfs_on_screen.end(), i) == acfs_on_screen.end())
		{
			float _x, _y, _z;
			_z =   3 * MAX_SAMP_Z + 100*i;
			_x =   3 * MAX_SAMP_Z + 100*i;
			_y =   3 * MAX_SAMP_Z + 100*i;

			CamframeToGlobal(_x, _y, _z, &mpacf[i]);
			mpacf[i].SetAircraftData();
		}
	}
}

static void mult_matrix_vec(float dst[4], const float m[16], const float v[4])
{
	dst[0] = v[0] * m[0] + v[1] * m[4] + v[2] * m[8] + v[3] * m[12];
	dst[1] = v[0] * m[1] + v[1] * m[5] + v[2] * m[9] + v[3] * m[13];
	dst[2] = v[0] * m[2] + v[1] * m[6] + v[2] * m[10] + v[3] * m[14];
	dst[3] = v[0] * m[3] + v[1] * m[7] + v[2] * m[11] + v[3] * m[15];
}


bool AcfonScreen(const float* pos, const float* mv, const float* proj)
{
	// Simulate the OpenGL transformation to get screen coordinates.

	float acf_eye[4], acf_ndc[4];
	mult_matrix_vec(acf_eye, mv, pos);
	mult_matrix_vec(acf_ndc, proj, acf_eye);

	acf_ndc[3] = 1.0f / acf_ndc[3];
	acf_ndc[0] *= acf_ndc[3];
	acf_ndc[1] *= acf_ndc[3];
	acf_ndc[2] *= acf_ndc[3];


	float final_x = rec_var_ptr->screen_w * (acf_ndc[0] * 0.5f + 0.5f);
	float final_y = rec_var_ptr->screen_h * (acf_ndc[1] * 0.5f + 0.5f);

	float d = sqrt(pow(pos[0] - cam_ptr->x, 2) + pow(pos[1] - cam_ptr->y, 2) + pow(pos[2] - cam_ptr->z, 2));

	bool my_res = ((final_x > 0) && (final_x < rec_var_ptr->screen_w) && (final_y > 0) && (final_y < rec_var_ptr->screen_h) && (acf_eye[2] < 0) && d < 4.0 * MAX_SAMP_Z);

	return my_res;
}

bool CheckOutAcfs()
{
	/*Assuming all the Alive acfs are mp and under ai control*/
	int tot_acf, tot_alive_acf;

	XPLMCountAircraft(&tot_acf, &tot_alive_acf, NULL);
	tot_alive_acf--;

	bool result = false;
	float mv[16], proj[16];

	// Read the model view and projection matrices from this frame
	XPLMGetDatavf(s_matrix_wrl, mv, 0, 16);
	XPLMGetDatavf(s_matrix_proj, proj, 0, 16);
	acfs_on_screen.clear();
	if (rec_var_ptr->capturing_state == USRS || rec_var_ptr->capturing_state == USRM) {
		float user_x, user_y, user_z;
		user_x = XPLMGetDataf(gPlaneX);
		user_y = XPLMGetDataf(gPlaneY);
		user_z = XPLMGetDataf(gPlaneZ);
		float pos[4] = { user_x, user_y, user_z, 1.0f };
		result = result || AcfonScreen(pos, mv, proj);

		if (rec_var_ptr->capturing_state == USRS) return result;
	}

	for (int i = 0; i < tot_alive_acf; ++i)
	{
		mpacf[i].GetAircraftData();

		if (mpacf[i].plane_x == 0.0 && mpacf[i].plane_y == 0.0 && mpacf[i].plane_z == 0.0)
			continue;

		// Read the ACF's OpengL coordinates
		float acf_wrl[4] = { mpacf[i].plane_x, mpacf[i].plane_y, mpacf[i].plane_z, 1.0f };

		bool my_res = AcfonScreen(acf_wrl, mv, proj);
		if (my_res) {
			acfs_on_screen.push_back(i);
		}

		result = (result || my_res);
	}
	return result;
}

void AdvanceCollisionCam(void) {
	double ux, uy, uz, uvx, uvy, uvz, uax, uay, uaz;
	double est_x, est_y, est_z;
	double cx, cy, cz, cvx, cvy, cvz;

	ux = XPLMGetDataf(gPlaneX);
	uy = XPLMGetDataf(gPlaneY);
	uz = XPLMGetDataf(gPlaneZ);
	uvx = XPLMGetDataf(gPlaneVX);
	uvy = XPLMGetDataf(gPlaneVY);
	uvz = XPLMGetDataf(gPlaneVZ);
	uax = XPLMGetDataf(gPlaneAX);
	uay = XPLMGetDataf(gPlaneAY);
	uaz = XPLMGetDataf(gPlaneAZ);

	float t = cam_ptr->init_time + cam_ptr->CAMMAXTIME - XPLMGetElapsedTime();
	est_x = ux + uvx * t + 0.5 * uax * pow(t, 2);
	est_y = uy + uvy * t + 0.5 * uay * pow(t, 2);
	est_z = uz + uvz * t + 0.5 * uaz * pow(t, 2);

	float dt = XPLMGetElapsedTime() - cam_ptr->CLASTTIME;

	cvx = (est_x - cam_ptr->x) / t;
	cvy = (est_y - cam_ptr->y) / t;
	cvz = (est_z - cam_ptr->z) / t;
	cam_ptr->cvx = cam_ptr->cvx + KPCOL * (cvx - cam_ptr->cvx);
	cam_ptr->cvy = cam_ptr->cvy + KPCOL * (cvy - cam_ptr->cvy);
	cam_ptr->cvz = cam_ptr->cvz + KPCOL * (cvz - cam_ptr->cvz);

	cam_ptr->x += cam_ptr->cvx * dt;
	cam_ptr->y += cam_ptr->cvy * dt;
	cam_ptr->z += cam_ptr->cvz * dt;

	cam_ptr->roll = 0.0;
	cam_ptr->pitch = atan(cvy / sqrt(pow(cvx, 2) + pow(cvz, 2))) / RADF;
	float ang_x = atan2(cvz, cvx);
	cam_ptr->head = M_PI/2 + ang_x > 0 ? M_PI/2 + ang_x : 5 * M_PI/2 + ang_x;
	cam_ptr->head /=RADF;

	cam_ptr->CLASTTIME = XPLMGetElapsedTime();

}
void AdvanceOwnshipPosition() {
	double ux, uy, uz, uvx, uvy, uvz, uax, uay, uaz;

	ux = XPLMGetDataf(gPlaneX);
	uy = XPLMGetDataf(gPlaneY);
	uz = XPLMGetDataf(gPlaneZ);

	cam_ptr->x = ux;
	cam_ptr->y = uy;
	cam_ptr->z = uz;

	float curr_prop_deg[1];
	float next_prop_deg[1];
	XPLMGetDatavf(gPropRot, curr_prop_deg, 0, 1);
	float t_now = XPLMGetElapsedTime();

	next_prop_deg[0] = curr_prop_deg[0] + GetRandomRange(300.0, 400.0) * (cam_ptr->CLASTTIME - t_now);
	next_prop_deg[0] = (next_prop_deg[0] < 360.0) ? next_prop_deg[0] : next_prop_deg[0] - 360.0;
	XPLMSetDatavf(gPropRot, next_prop_deg, 0, 1);
	cam_ptr->CLASTTIME = XPLMGetElapsedTime();
}

void AdvancePosition()
{
	if (!XPLMGetDatai(sim_paused))
	{
		if (rec_var_ptr->capturing_state == USRC) {
			AdvanceCollisionCam();
		}
		else{

			float dt = XPLMGetElapsedTime() - cam_ptr->CLASTTIME;
			float mv[16];
			XPLMGetDatavf(s_matrix_wrl, mv, 0, 16);

			Eigen::Matrix<float, 4, 4, Eigen::ColMajor> Mview(mv);
			auto Mviewinv = Mview.inverse();
			Eigen::Vector4f next_cam_pos_local(0.0, 0.0, - cam_ptr->vz * dt, 1.0);
			Eigen::Vector4f next_cam_pos_global = Mviewinv * next_cam_pos_local;
			
			cam_ptr->x = next_cam_pos_global[0];
			cam_ptr->y = next_cam_pos_global[1];
			cam_ptr->z = next_cam_pos_global[2];
		
			if (cam_ptr->y < cam_ptr->CMINY) {
				cam_ptr->dpitch = cam_ptr->CMAXDP;
			}

			float rn, pn, hn;
			rn = cam_ptr->roll + cam_ptr->droll * dt;
			pn = cam_ptr->pitch + cam_ptr->dpitch * dt;
			hn = cam_ptr->head + cam_ptr->dhead * dt;

			if (rec_var_ptr->capturing_state != USRC) {
				cam_ptr->roll = rn> cam_ptr->CMINR && rn < cam_ptr->CMAXR ? rn : (rn > cam_ptr->CMAXR ? cam_ptr->CMAXR : cam_ptr->CMINR);
				cam_ptr->pitch = pn > cam_ptr->CMINP && pn < cam_ptr->CMAXP ? pn : (pn > cam_ptr->CMAXP ? cam_ptr->CMAXP : cam_ptr->CMINP);
				cam_ptr->head = hn > cam_ptr->CMINH && hn < cam_ptr->CMAXH ? hn : (hn > cam_ptr->CMAXH ? cam_ptr->CMAXH : cam_ptr->CMINH);
			}
		
			cam_ptr->CLASTTIME = XPLMGetElapsedTime();
		}
	}

	else
	{
		cam_ptr->CLASTTIME = XPLMGetElapsedTime();
	}
}

bool DoCamReset()
{
	bool c0 = !cam_ptr->init;
	bool c1 = false;//!CheckOutAcfs() && !(rec_var_ptr->capturing_state == USRS || rec_var_ptr->capturing_state == USRM);
	bool c2 = XPLMGetElapsedTime() - cam_ptr->init_time  > cam_ptr->CAMMAXTIME + 4.0;
	bool c3 = global_image_count % rec_var_ptr->frame_per_seq == 0 && global_image_count !=0 && rec_var_ptr->capturing_state!=USRC;
	bool c4 = CaminitTimeDelta(0.6);
	bool c5 = rec_var_ptr->capturing_state == USRC && (cam_ptr->init_time + cam_ptr->CAMMAXTIME - XPLMGetElapsedTime() < 0.1);
	
	bool c_f = c0 || ((c1 || c2 || c3 || c5) && !c4);
	/*DEBUGACF << "Should Reset Cam? " << c_f << " cond: " << c0 << ", " << c1 << "," << c2 << ", " << c3 << ", "<< c4 << " | Global image count:" << global_image_count << "| Sequence: " << cam_ptr -> seq<<  "\n";
	XPLMDebugString((char*)DEBUGACF.str().c_str());
	DEBUGACF.str("");*/

	return c_f;
}


int 	CameraControl(
	XPLMCameraPosition_t* outCameraPosition,
	int                  inIsLosingControl,
	void* inRefcon)
{
	if (outCameraPosition && !inIsLosingControl)
	{
		if (cam_ptr->reset)
		{
			switch (rec_var_ptr->capturing_state)
			{
				case (USRM):
					ResetCameraAroundUser();
					ResetAcfs();
					break;
				case (USRS):
					ResetCameraAroundUser();
					break;
				case (USRC):
					ResetCameraAroundUser();
					break;
				case (NONSEQ || SEQCAMFIXED):
					ResetCamera();
					ResetAcfs();
					break;

				default:
					ResetCamera();
					ResetAcfs();
					break;
			}

			/* Fill out the camera position info. */
			outCameraPosition->x = cam_ptr->x;
			outCameraPosition->y = cam_ptr->y;
			outCameraPosition->z = cam_ptr->z;
			outCameraPosition->pitch = cam_ptr->pitch;
			outCameraPosition->heading = cam_ptr->head;
			outCameraPosition->roll = cam_ptr->roll;
		}

		else if (cam_ptr->init)
		{
			AdvancePosition();
			outCameraPosition->x = cam_ptr->x;
			outCameraPosition->y = cam_ptr->y;
			outCameraPosition->z = cam_ptr->z;
			outCameraPosition->pitch = cam_ptr->pitch;
			outCameraPosition->heading = cam_ptr->head;
			outCameraPosition->roll = cam_ptr->roll;
		}
		
		if (DoCamReset()) {
			if (cam_ptr->seq % rec_var_ptr->seq_req == 0 && cam_ptr->seq != 0 && cam_ptr->init && rec_var_ptr->capturing_state!=USRC) {
				XPLMRegisterFlightLoopCallback(ConfigureEnv, -1, NULL);
				cam_ptr->reset = true;
				cam_ptr->init = false;
				return 1;
			} 
			if (cam_ptr->seq > rec_var_ptr->seq_req && rec_var_ptr->capturing_state == USRC) {
				XPLMDebugString("Not controlling camera for collider seq\n");
				XPLMDontControlCamera();
				XPLMRegisterFlightLoopCallback(DestroyRecorders, -1, NULL);
			}
			cam_ptr->reset = true;
			cam_ptr->init = false;
		}
	}
	/* Return 1 to indicate we want to keep controlling the camera. */
	return 1;
}

float OwnshipCollider(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon) {
	if (cam_ptr->reset) {
		ResetAcfs();
		ResetCameraAroundUser();
		float ap_vv;
		if (XPLMGetDataf(gPlaneY) < 200.0) {
			ap_vv = GetRandomRange(0, 100);
		}
		else {
			ap_vv = GetRandomRange(-400, 100);
		}
		float ap_hd = GetRandomRange(0, 359);
		XPLMSetDataf(gApHead, ap_hd);
		XPLMSetDataf(gApVerticalVel, ap_vv);
	}
	else {
		AdvanceOwnshipPosition();
	}
	if (DoCamReset()) {
		if (cam_ptr->seq > rec_var_ptr->seq_req) {
			XPLMRegisterFlightLoopCallback(DestroyRecorders, -1, NULL);
		}
		cam_ptr->reset = true;
		cam_ptr->init = false;
	}
	return -1.0;
}

float CameraControlSeqAcf(float                inElapsedSinceLastCall,
	float                inElapsedTimeSinceLastFlightLoop,
	int                  inCounter,
	void* inRefcon)
{

	if (DoCamReset())
	{
		cam_ptr->GetCameraPosition();
		ResetAcfs();
	}
	/*To be callled in every Frame*/
	return -1;
}

void MenuHandler(void* in_menu_ref, void* in_item_ref)
{
	if (!strcmp((const char*)in_item_ref, "Menu Item 1"))
	{
		SetCameraPtr();
		rec_var_ptr->capturing_state = NONSEQ;
		XPLMControlCamera(xplm_ControlCameraForever, CameraControl, NULL);
		XPLMRegisterFlightLoopCallback(PauseSimRec, START_T, NULL);
		XPLMRegisterFlightLoopCallback(RecordDataCallback, 0.0, NULL);
		XPLMRegisterFlightLoopCallback(UnPauseSimRec, 0.0, NULL);
	}
	else if (!strcmp((const char*)in_item_ref, "Menu Item 2"))
	{
		SetCameraPtr();
		rec_var_ptr->capturing_state = SEQCAMFIXED;
		XPLMControlCamera(xplm_ControlCameraForever, CameraControl, NULL);
		XPLMRegisterFlightLoopCallback(PauseSimRec, START_T, NULL);
		XPLMRegisterFlightLoopCallback(RecordDataCallback, 0.0, NULL);
		XPLMRegisterFlightLoopCallback(UnPauseSimRec, 0.0, NULL);
	}
	else if (!strcmp((const char*)in_item_ref, "Menu Item 3"))
	{
		SetCameraPtr();
		rec_var_ptr->capturing_state = SEQACF;
		XPLMRegisterFlightLoopCallback(CameraControlSeqAcf, 1, NULL);
		XPLMRegisterFlightLoopCallback(PauseSimRec, START_T, NULL);
		XPLMRegisterFlightLoopCallback(RecordDataCallback, 0.0, NULL);
		XPLMRegisterFlightLoopCallback(UnPauseSimRec, 0.0, NULL);
	}
	else if (!strcmp((const char*)in_item_ref, "Menu Item 4"))
	{
		cam_ptr->ResetCameraVars();
		rec_var_ptr->capturing_state = USRM;
		env_var_ptr->initial_x = XPLMGetDataf(gPlaneX);
		env_var_ptr->initial_y = XPLMGetDataf(gPlaneY);
		env_var_ptr->initial_z = XPLMGetDataf(gPlaneZ);

		SetCameraPtr();
		XPLMRegisterFlightLoopCallback(ConfigureEnv, -1, NULL);

	}
	else if (!strcmp((const char*)in_item_ref, "Menu Item 5"))
	{
		cam_ptr->ResetCameraVars();
		rec_var_ptr->capturing_state = USRS;
		env_var_ptr->initial_x = XPLMGetDataf(gPlaneX);
		env_var_ptr->initial_y = XPLMGetDataf(gPlaneY);
		env_var_ptr->initial_z = XPLMGetDataf(gPlaneZ);
		SetCameraPtr();
		XPLMRegisterFlightLoopCallback(ConfigureEnv, -1, NULL);
	}

	else if (!strcmp((const char*)in_item_ref, "Menu Item 6"))
	{
		cam_ptr->ResetCameraVars();
		rec_var_ptr->capturing_state = USRC;
		env_var_ptr->initial_x = XPLMGetDataf(gPlaneX);
		env_var_ptr->initial_y = XPLMGetDataf(gPlaneY);
		env_var_ptr->initial_z = XPLMGetDataf(gPlaneZ);
		SetCameraPtr();
		XPLMRegisterFlightLoopCallback(ConfigureEnv, -1, NULL);
	}
	else if (!strcmp((const char*)in_item_ref, "Menu Item 7"))
	{
		int vals[] = { 1 };
		int nvals[] = { 0 };
		XPLMSetDatavi(ov_prop_disc, vals, 0, 1);
		XPLMSetDatavi(gPropIsDisc, nvals, 0, 1);
		cam_ptr->ResetCameraVars();
		rec_var_ptr->capturing_state = OWNC;
		env_var_ptr->initial_x = XPLMGetDataf(gPlaneX);
		env_var_ptr->initial_y = XPLMGetDataf(gPlaneY);
		env_var_ptr->initial_z = XPLMGetDataf(gPlaneZ);
		SetCameraPtr();
		XPLMRegisterFlightLoopCallback(ConfigureEnv, -1, NULL);
	}
}

PLUGIN_API int XPluginEnable()
{
	return 1;
}

PLUGIN_API void XPluginStop()
{
	XPLMUnregisterFlightLoopCallback(RecordDataCallback, NULL);
	XPLMUnregisterFlightLoopCallback(PauseSimRec, NULL);
	XPLMUnregisterFlightLoopCallback(UnPauseSimRec, NULL);
	XPLMDontControlCamera();

}

PLUGIN_API void XPluginDisable()
{
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMessage, void* inParam)
{
}