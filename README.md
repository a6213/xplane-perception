# xplane-perception
Plugin to collect perception data of aircrafts from X-Plane 11

# Generate ground truth bounding boxes and masks
- Place the 3rd party plugins under `Resources/plugins` within the X-Plane 11 installation
- Compile the XpDataGen plugin and place the module under the same folder
- Run X-Plane and select a recording option from the Plugins menu during flight